package com.appnosystem;

import java.util.ArrayList;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AccountList extends ListActivity {

	private ArrayAdapter<String> accountAdapter;
	private AccountManager accountManager;
	private Account[] googleAccounts;
	private List<String> items;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.accounts_list);
		accountManager = AccountManager.get(this);
		googleAccounts = accountManager.getAccountsByType("com.google");
		items = new ArrayList<String>();
		for (int i = 0; i < googleAccounts.length; ++i) {
			items.add(googleAccounts[i].name);
		}
		accountAdapter = new ArrayAdapter<String>(this, R.layout.account_row,
				items);
		setListAdapter(accountAdapter);
	}

	public void onListItemClick(ListView l, View v, int position, long id) {
		String accountName = accountAdapter.getItem(position);
		Intent extras = new Intent();
		extras.putExtra("account", accountName);
		setResult(RESULT_OK, extras);
		finish();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// go back means account selection cancel 
			setResult(RESULT_CANCELED);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
