package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/**
 * This class manage the current user's status and location.
 * 
 * @author ANS
 * 
 */
public class StatusActivity extends AnsBaseActivity implements OnClickListener {

	private static final String[] AVAILABILITY_ITEMS = { "Available", "Busy",
			"Appear Offline" };
	private static final String[] LOCATION_ITEMS = { "", "CB 1", "CB 2", "LNG",
			"SIT Building", "SIT Library", "KMUTT Library", "Other..." };

	private Button availabilityImageButton;
	private Button placesImageButton;

	private ImageView profilePicture;

	private TextView fullNameText;
	private TextView usernameText;
	private TextView availabilityText;
	private TextView locationText;
	
	private EditText otherplaceEditText;

	private ActionMode mMode;

	private int previousAvailability;
	private int availability;

	private String previousLocation;

	private ImageLoader imageLoader;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.status_layout);
		getSupportActionBar().setTitle("Status");

		fullNameText = (TextView) findViewById(R.id.full_name_value);
		usernameText = (TextView) findViewById(R.id.username_value);
		availabilityText = (TextView) findViewById(R.id.availability_value);
		locationText = (TextView) findViewById(R.id.location_value);
		profilePicture = (ImageView) findViewById(R.id.profile_picture);
		
		otherplaceEditText = (EditText) findViewById(R.id.other_place);
		otherplaceEditText.setVisibility(EditText.GONE);
		imageLoader = new ImageLoader(this);
		imageLoader.displayImage(AnsUtilities.getProfilePicture(this),
				profilePicture);

		// Set information of current user.
		fullNameText.setText(AnsUtilities.getFullName(this));
		usernameText.setText(AnsUtilities.getUsername(this));
		locationText.setText(AnsUtilities.getLocation(this));
		availabilityText.setText(AnsUtilities.getAvailability(this));

		availabilityImageButton = (Button) findViewById(R.id.availability_button);
		availabilityImageButton.setOnClickListener(this);

		placesImageButton = (Button) findViewById(R.id.place_button);
		placesImageButton.setOnClickListener(this);

		for (int i = 0; i < AVAILABILITY_ITEMS.length; i++) {
			if (availabilityText.getText().toString()
					.equals(AVAILABILITY_ITEMS[i])) {
				previousAvailability = i;
			}
		}
		previousLocation = locationText.getText().toString();
	}

	public void onClick(View v) {
		int viewId = v.getId();
		switch (viewId) {
		case R.id.availability_button:
			createAvailabilityDialog();
			break;
		case R.id.place_button:
			createPlacesDialog();
			break;
		}
	}

	public void createAvailabilityDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Availability");
		builder.setIcon(R.drawable.ic_dialog_availability);
		builder.setItems(AVAILABILITY_ITEMS,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int item) {
						mMode = startActionMode(new StatusActionMode());

						availabilityText.setText(AVAILABILITY_ITEMS[item]);
						// new UserProfileTask().execute();
						availability = item;
					}
				});
		builder.show();
	}

	public void createPlacesDialog() {
		// final String[] items = { "-", "CB2", "SIT Buliding", "LNG" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Places");
		builder.setIcon(R.drawable.ic_dialog_place);
		builder.setItems(LOCATION_ITEMS, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				mMode = startActionMode(new StatusActionMode());
				// 7 is other
				if (item == 7) {
					locationText.setText("7");
					otherplaceEditText.setVisibility(EditText.VISIBLE);
				} else {
					locationText.setText(LOCATION_ITEMS[item]);
				}
			}
		});
		builder.show();
	}

	private final class StatusActionMode implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.add("Ok").setShowAsAction(
					MenuItem.SHOW_AS_ACTION_IF_ROOM);
			menu.add("Cancel").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			if (item.getTitle().equals("Ok")) {
				Toast.makeText(getApplicationContext(), "Save",
						Toast.LENGTH_LONG).show();
				new UserProfileTask().execute();
			} else if (item.getTitle().equals("Cancel")) {
				otherplaceEditText.setVisibility(EditText.INVISIBLE);
				locationText.setText(previousLocation);
				availabilityText
						.setText(AVAILABILITY_ITEMS[previousAvailability]);
			}
			if (mMode != null) {
				mode.finish();
			}
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {

		}
	}

	private class UserProfileTask extends AsyncTask<String, Void, String> {
		private String result;

		@Override
		protected String doInBackground(String... params) {
			result = changeProfile();
			return result;
		}

		private String changeProfile() {
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			nameValuePairs
					.add(new BasicNameValuePair("change_profile", "true"));
			nameValuePairs.add(new BasicNameValuePair("availability", String
					.valueOf(availability)));
			nameValuePairs.add(new BasicNameValuePair("place", locationText
					.getText().toString()));
			HttpPost httpPost = new HttpPost("http://appnosystem.com/api/user/");

			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpPost.setHeader("Authorization", apiKey);
			String responseString = "";
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						HTTP.UTF_8));
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpPost);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				Log.i("change profile", responseString);
			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {

			try {
				JSONObject userInformation = new JSONObject(result);
				String username = userInformation.getString("username");
				String firstName = userInformation.getString("first_name");
				String lastName = userInformation.getString("last_name");
				String email = userInformation.getString("email");
				String availability = userInformation.getString("availability");
				String profilePicture = userInformation
						.getString("profile_picture_url");
				String location = userInformation.getString("location");

				AnsUtilities.setUsername(getApplicationContext(), username);
				AnsUtilities.setFullName(getApplicationContext(), firstName
						+ " " + lastName);
				AnsUtilities.setEmail(getApplicationContext(), email);
				AnsUtilities.setAvailability(getApplicationContext(),
						availability);
				// TODO workaround here
				AnsUtilities.setProfilePicture(getApplicationContext(),
						AnsUser.appendCroppedUrl(profilePicture));
				AnsUtilities.setLocation(getApplicationContext(), location);
				Log.i("update profile complete", userInformation.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}
}
