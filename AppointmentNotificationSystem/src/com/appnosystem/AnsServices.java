package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.Log;
import android.widget.Toast;

public class AnsServices extends IntentService {

	private SharedPreferences settings;

	// actions
	public static final String SEND_REGISTRATION_ID = "send_reg_id";
	public static final String REGISTRATION_ID = "registration_id";
	public static final String WEBAPP_LOGIN = "login_to_django_server";
	public static final String SEND_MESSAGE_TO = "send_message_to";

	public static final String GET_MESSAGE = "get_message";
	public static final String MESSAGE_ID = "message_id";

	// URL Strings
	public static final String APIKEY_URL = "http://appnosystem.com/api/apikey/";
	public static final String REGISTRATION_URL = "http://appnosystem.com/api/register/";
	public static final String USER_URL = "http://appnosystem.com/api/user/";
	public static final String SEND_MESSAGE_TO_URL = "http://appnosystem.com/api/send_message_to/";

	public static final String C2DM_SENDER = "emergencynosystem@gmail.com";
	public static final String C2DM_ACCOUNT_EXTRA = "account_name";
	public static final String C2DM_MESSAGE_EXTRA = "message";
	public static final String C2DM_MESSAGE_SYNC = "sync";

	public static final int STATUS_RUNNING = 0;
	public static final int STATUS_FINISHED = 1;
	public static final int STATUS_ERROR = 2;

	private Context applicationContext;

	public Handler toastHandler;

	public AnsServices() {
		super("AnsPreferences");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		toastHandler = new Handler();
		settings = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// String command = intent.getStringExtra("command");
		String action = intent.getAction();
		if (action.equals(AnsServices.WEBAPP_LOGIN)) {
			final ResultReceiver receiver = intent
					.getParcelableExtra("receiver");

			String username = intent.getStringExtra("username");
			String password = intent.getStringExtra("password");

			Bundle b = new Bundle();
			receiver.send(STATUS_RUNNING, Bundle.EMPTY);
			try {
				// get some data or process something
				// b.putParcelable("result", "");
				authenticate(username, password);
				receiver.send(STATUS_FINISHED, b);
			} catch (Exception e) {
				b.putString(Intent.EXTRA_TEXT, e.toString());
				receiver.send(STATUS_ERROR, b);
			}
		} else if (action.equals(AnsServices.SEND_REGISTRATION_ID)) {
			AnsBaseActivity ansReference = AnsBaseActivity.getReference();
			Account account = ansReference.getSelectedAccount();
			sendRegistrationID(
					intent.getStringExtra(AnsServices.REGISTRATION_ID),
					account, getContentResolver());
		} else if (action.equals(AnsServices.SEND_MESSAGE_TO)) {
			String recipient = intent.getStringExtra("recipient");
			String message = intent.getStringExtra("message_body");
			String agenda = intent.getStringExtra("agenda");
			String dateValue = intent.getStringExtra("date_value");
			String timeValue = intent.getStringExtra("time_value");
			String place = intent.getStringExtra("place");
			sendMessageTo(recipient, message, agenda, dateValue, timeValue, place);
			toastHandler.post(new ToastText("Your message were sent!"));
		}
		this.stopSelf();
		// handleIntent(intent);
	}

	public void authenticate(String username, String password) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		httpClient.getCredentialsProvider().setCredentials(
				new AuthScope(null, -1),
				new UsernamePasswordCredentials(username, password));

		HttpPost httpPost = new HttpPost(USER_URL);

		// Before make API key authentication, Android client must get the API
		// key first.
		String apiKey = getApiKeyFromServer(username, password);
		if (apiKey.length() != 50) {
			toastHandler.post(new ToastText(apiKey));
			return;
		}
		httpPost.setHeader("Authorization", apiKey);

		String responseString = "";
		JSONObject userInformation;
		try {
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			Log.e("authenticate", responseString);
			try {
				userInformation = new JSONObject(responseString);
				setUserInformation(apiKey, userInformation);
			} catch (JSONException e) {
				Log.e("JSON userInformation", e.toString());
			}

		} catch (ClientProtocolException e) {

		} catch (IOException e) {

		}
	}

	public void sendRegistrationID(String registrationId, Account account,
			ContentResolver contentResolver) {
		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("phone_id",
				getPhoneId(contentResolver)));
		nameValuePairs.add(new BasicNameValuePair(
				AnsServices.C2DM_ACCOUNT_EXTRA, account.name));
		nameValuePairs.add(new BasicNameValuePair("registration_id",
				registrationId));
		nameValuePairs.add(new BasicNameValuePair("username", getUsername()));

		HttpPost httpPost = new HttpPost(REGISTRATION_URL);
		httpPost.setHeader("Authorization", getApiKey());
		String responseString = "";
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			Toast.makeText(applicationContext, responseString,
					Toast.LENGTH_LONG).show();
			Log.e("register", responseString);
		} catch (Exception e) {

		}
	}

	public void sendMessageTo(String recipient, String message, String agenda,
			String dateValue, String timeValue, String place) {
		HttpPost httpPost = new HttpPost(SEND_MESSAGE_TO_URL + recipient + "/");
		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("message_body", message));

		nameValuePairs.add(new BasicNameValuePair("agenda", agenda));
		nameValuePairs.add(new BasicNameValuePair("date_value", dateValue));
		nameValuePairs.add(new BasicNameValuePair("time_value", timeValue));
		nameValuePairs.add(new BasicNameValuePair("place", place));
		nameValuePairs.add(new BasicNameValuePair("collapse_key", "message"));
		// Messages sent from Android will never be a missed call message
		nameValuePairs.add(new BasicNameValuePair("missed_call", "false"));
		Log.e("nameValuePairs", nameValuePairs.toString());
		String apiKey = AnsUtilities.getApiKey(getApplicationContext());
		httpPost.setHeader("Authorization", apiKey);

		String responseString = "";
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			Log.e("send message to", responseString);
		} catch (Exception e) {

		}

	}

	public String getAPIKeyResponse(String url) {
		DefaultHttpClient httpClient = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost(url);
		String apiKey = getApiKey();
		httpPost.setHeader("Authorization", apiKey);

		String responseString = "";
		try {
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
		} catch (ClientProtocolException e) {

		} catch (IOException e) {

		}
		return responseString;
	}

	public String getApiKey() {
		return settings.getString("api_key", "api_key");
	}

	public String getApiKeyFromServer(String username, String password) {
		DefaultHttpClient httpClient = new DefaultHttpClient();

		httpClient.getCredentialsProvider().setCredentials(
				new AuthScope(null, -1),
				new UsernamePasswordCredentials(username, password));
		HttpPost httpPost = new HttpPost(APIKEY_URL);
		String responseString = "";
		try {
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			Log.e("get api key", responseString);
		} catch (ClientProtocolException e) {

		} catch (IOException e) {

		}
		return responseString;
	}

	public String getUsername() {
		return settings.getString("username", "username");
	}

	private String getPhoneId(ContentResolver contentResolver) {
		String phoneId = System.getString(contentResolver, Secure.ANDROID_ID);
		if (phoneId == null) {
			phoneId = "emulator"; // some emulators return null
		}
		return phoneId;
	}


	/**
	 * Set user information
	 * 
	 * @param userInformation
	 */
	private void setUserInformation(String apiKey, JSONObject userInformation) {
		try {
			String username = userInformation.getString("username");
			String firstName = userInformation.getString("first_name");
			String lastName = userInformation.getString("last_name");
			String email = userInformation.getString("email");
			String availability = userInformation.getString("availability");
			String profilePicture = userInformation.getString("profile_picture_url");
			String location = userInformation.getString("location");
			AnsUtilities.setApiKey(this, apiKey);
			AnsUtilities.setUsername(this, username);
			AnsUtilities.setFullName(this, firstName + " " + lastName);
			AnsUtilities.setEmail(this, email);
			AnsUtilities.setAvailability(this, availability);
			// TODO workaround here
			AnsUtilities.setProfilePicture(this, AnsUser.appendCroppedUrl(profilePicture));
			AnsUtilities.setLocation(this, location);
			// toastHandler.post(new ToastText(userInformation.toString(4)));
			Log.i("set user info", "set user info");
			Log.e("availability", AnsUtilities.getAvailability(this));
		} catch (JSONException e) {

		}
	}

	private class ToastText implements Runnable {
		private String text;

		public ToastText(String text) {
			this.text = text;
		}

		@Override
		public void run() {
			Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG)
					.show();
		}
	}
}
