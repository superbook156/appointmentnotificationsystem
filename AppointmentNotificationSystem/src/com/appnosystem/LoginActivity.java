package com.appnosystem;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AnsBaseActivity implements OnClickListener,
		AnsResultReceiver.Receiver {

	private EditText usernameEditText;
	private EditText passwordEditText;
	private Button signinButton;

	private String usernameString;
	private String passwordString;

	public AnsResultReceiver receiver;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_page);

		receiver = new AnsResultReceiver(new Handler());
		receiver.setReceiver(this);

		usernameEditText = (EditText) findViewById(R.id.usernameEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		signinButton = (Button) findViewById(R.id.btn_signin);
		gotoMain();
		signinButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		usernameString = usernameEditText.getText().toString();
		passwordString = passwordEditText.getText().toString();
		// Toast.makeText(this, usernameString, Toast.LENGTH_LONG).show();
		// new LoginTask(this).execute(usernameString, passwordString);
		// authenticate(usernameString, passwordString);
		authenticate(usernameString, passwordString); // Awesome
	}

	public void authenticate(String username, String password) {
		final Intent loginIntent = new Intent(Intent.ACTION_SYNC, null, this,
				AnsServices.class);
		loginIntent.putExtra("receiver", receiver);
		loginIntent.setAction(AnsServices.WEBAPP_LOGIN);
		// loginIntent.putExtra("command", AnsServices.WEBAPP_LOGIN);
		loginIntent.putExtra("username", username);
		loginIntent.putExtra("password", password);
		startService(loginIntent);
		progressDialog.setMessage("Logging in...");
	}

	public void onPause() {
		super.onPause();
		receiver.setReceiver(null);
		progressDialog.dismiss();
	}

	public void onResume() {
		super.onResume();
		receiver.setReceiver(this); // lol
	}

	public void onReceiveResult(int resultCode, Bundle resultData) {
		switch (resultCode) {
		case AnsServices.STATUS_RUNNING:
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			progressDialog.show();
			break;
		case AnsServices.STATUS_FINISHED:
			// GCM register here maybe
			Log.e("Go main", "Try to go main");
			gotoMain();
			progressDialog.hide();
			break;
		case AnsServices.STATUS_ERROR:
			break;
		}
	}

	private void gotoMain() {
		if (AnsUtilities.isAuthenticated(this)) {
			Intent i = new Intent(getApplicationContext(), MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
//			finish();
		} else {
			return;
		}
	}

}
