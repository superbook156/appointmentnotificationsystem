package com.appnosystem;

import org.json.JSONException;
import org.json.JSONObject;

public class AnsUser {

	private String username;
	private String asteriskUsername;
	private String firstName;
	private String lastName;
	private String email;
	private int status;
	private int userType;
	private String profilePicture;

	public AnsUser(JSONObject user) {
		try {
			this.username = user.getString("username");
			// this.asteriskUsername = user.getString("asterisk_username");
			this.asteriskUsername = "Asterisk";
			this.firstName = user.getString("first_name");
			this.lastName = user.getString("last_name");
			this.email = user.getString("email");
			this.profilePicture = user.getString("profile_picture_url");
			this.status = user.getInt("status");
			this.userType = 0; // think later TODO
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public AnsUser(String profilePicture, String username,
			String asteriskUsername, String firstName, String lastName,
			String email, int status, int userType) {
		this.profilePicture = profilePicture;
		this.username = username;
		this.asteriskUsername = asteriskUsername;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.status = status;
		this.userType = userType;
	}

	public String getUsername() {
		return username;
	}

	public String getAsteriskUsername() {
		return asteriskUsername;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public int getStatus() {
		return status;
	}

	/**
	 * Returns hard coded to change URL to easy_thumbnails 149x149 size.
	 * 
	 * @return URL of profile picture crop 149x149
	 */
	public String getProfilePictureUrl() {
		return profilePicture + ".149x149_q85_crop.jpg";
	}
	
	public static String appendCroppedUrl(String profilePictureUrl) {
		return profilePictureUrl + ".149x149_q85_crop.jpg";
	}

	/**
	 * Returns availability of user as string.
	 * 
	 * @return availability string
	 */
	public String getAvailability() {
		if (status == 0) {
			return "Available";
		} else if (status == 1) {
			return "Busy";
		}
		return "Appear Offline";
	}

	/**
	 * 
	 * @return
	 */
	public int getUserType() {
		return userType;
	}

	public String getFullname() {
		String fullName = firstName + " " + lastName;
		return fullName;
	}

	public String toString() {
		return firstName + " " + lastName;
	}
}
