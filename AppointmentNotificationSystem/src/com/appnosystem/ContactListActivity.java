package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sipdroid.sipua.ui.Receiver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

//change to Activity sub-class for a while
public class ContactListActivity extends AnsBaseListActivity implements
		OnRefreshListener<ListView> {

	private List<AnsUser> usersList;
	private ContactListAdapter adapter;

	private static final int CALL_SIP_ACTION = 0;
	private static final int SEND_MESSAGE_ACTION = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Contacts");
		usersList = new ArrayList<AnsUser>();
		adapter = new ContactListAdapter(this, usersList);

		listView.setOnRefreshListener(this);
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final String[] items = { "Call SIP", "Send message" };
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// AnsUser ansUser = (AnsUser) listView
				// .getItemAtPosition(position);
				AnsUser ansUser = usersList.get(position - 1);
				final String recipient = ansUser.getUsername();
				final String asteriskUsername = ansUser.getAsteriskUsername();
				builder.setTitle("Select action");

				builder.setItems(items, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case CALL_SIP_ACTION:
							// Intent sipdroidIntent = new Intent(
							// getApplicationContext(), Sipdroid.class);
							// startActivity(sipdroidIntent);
							Receiver.engine(getApplicationContext()).call(
									asteriskUsername, true);
							break;
						case SEND_MESSAGE_ACTION:

							Intent messageIntent = new Intent(
									getApplicationContext(),
									MessagingActivity.class);
							messageIntent
									.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							messageIntent.putExtra("recipient", recipient);
							startActivity(messageIntent);
							break;
						}
					}
				});
				builder.show();

			}
		});
		listView.setAdapter(adapter);
		listView.setRefreshing();
		new GetContactsTask().execute();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		super.onRefresh(refreshView);
		new GetContactsTask().execute();
	}

	private class GetContactsTask extends AsyncTask<String, Void, String> {
		private String result;

		@Override
		protected String doInBackground(String... params) {
			result = getContactsJSON();
			return result;
		}

		private String getContactsJSON() {
			// String username =
			// AnsUtilities.getUsername(getApplicationContext());
			HttpGet httpGet = new HttpGet("http://appnosystem.com/api/user/");

			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpGet.setHeader("Authorization", apiKey);

			String responseString = "";
			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpGet);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				// Log.e("get contacts", responseString);
			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {
			usersList.clear();
			adapter.notifyDataSetChanged();
			JSONArray users;

			try {
				users = new JSONArray(result);
				for (int i = 0; i < users.length(); i++) {
					JSONObject user = new JSONObject(users.get(i).toString());
					String profilePicture = user
							.getString("profile_picture_url");
					String firstName = user.getString("first_name");
					String lastName = user.getString("last_name");
					String username = user.getString("username");

					String asteriskUsername = user
							.getString("asterisk_username");
					int status = user.getInt("status");
					int userType = user.getInt("user_type");
					String email = user.getString("email");
					AnsUser u = new AnsUser(profilePicture, username,
							asteriskUsername, firstName, lastName, email,
							status, userType);
					usersList.add(u);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			listView.onRefreshComplete();
			super.onPostExecute(result);
		}
	}
}
