package com.appnosystem;

import java.io.InputStream;
import java.io.OutputStream;

public class BitmapUtils {
	public static void copyStream(InputStream inputStream,
			OutputStream outputStream) {
		final int bufferSize = 1024;
		try {
			byte[] bytes = new byte[bufferSize];
			for (;;) {
				int count = inputStream.read(bytes, 0, bufferSize);
				if (count == -1) {
					break;
				}
				outputStream.write(bytes, 0, count);
			}
		} catch (Exception ex) {

		}
	}
}
