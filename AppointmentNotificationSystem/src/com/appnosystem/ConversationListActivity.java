package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

public class ConversationListActivity extends AnsBaseListActivity implements
		OnRefreshListener<ListView>, OnItemClickListener {

	private String sender;
	private List<Message> conversationList;
	private ConversationListAdapter adapter;
	public ImageLoader imageLoader;
	private ImageView senderProfilePicture;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		titleView = inflater.inflate(R.layout.ans_conversation_title, null);
		getSupportActionBar().setTitle("Conversation");
		senderProfilePicture = (ImageView) titleView
				.findViewById(R.id.sender_profile_picture);
		getSupportActionBar().setCustomView(titleView);

		imageLoader = new ImageLoader(getApplicationContext());

		sender = getIntent().getExtras().getString("sender");
		conversationList = new ArrayList<Message>();
		adapter = new ConversationListAdapter(this, conversationList);

		listView.setOnRefreshListener(this);
		listView.setOnItemClickListener(this);

		listView.setAdapter(adapter);
		listView.setRefreshing();
		new GetConversationTask().execute();
		new GetProfilePictureTask().execute();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		super.onRefresh(refreshView);
		new GetConversationTask().execute();
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// Message selectedMessage = ((Message) listView
		// .getItemAtPosition(position));
		// position - 1 because it start from header
		Message selectedMessage = conversationList.get(position - 1);
		// Intent for ConversationDetailActivity
		Intent detailIntent = new Intent(getApplicationContext(),
				ConversationDetailActivity.class);
		detailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		detailIntent.putExtra("conversation_fullname", selectedMessage
				.getSender().getFullname());
		detailIntent.putExtra("username", selectedMessage.getSender()
				.getUsername());
		detailIntent.putExtra("conversation_body", selectedMessage.getBody());
		detailIntent.putExtra("conversation_date", selectedMessage.getDate());
		detailIntent.putExtra("conversation_time", selectedMessage.getTime());
		// check whether selected message have agenda or not
		if (selectedMessage.hasAgenda()) {
			detailIntent.putExtra("agenda_id", selectedMessage.getAgenda()
					.getId());
			detailIntent.putExtra("agenda_date", selectedMessage.getAgenda()
					.getDate());
			detailIntent.putExtra("agenda_time", selectedMessage.getAgenda()
					.getTime());
			detailIntent.putExtra("agenda_place", selectedMessage.getAgenda()
					.getPlace());
			detailIntent.putExtra("agenda_confirm", selectedMessage.getAgenda()
					.getConfirmation());

		}
		// check wheter selected message have missed call photo or not
		if (selectedMessage.hasMissedCallPhoto()) {
			detailIntent.putExtra("missed_call_photo_url",
					selectedMessage.getMissedCallPhotoUrl());
			Log.e("if", selectedMessage.getMissedCallPhotoUrl());
		} else {
			detailIntent.putExtra("missed_call_photo_url", "");
			Log.e("else", selectedMessage.getMissedCallPhotoUrl());
		}
		detailIntent.putExtra("has_agenda", selectedMessage.hasAgenda());
		startActivity(detailIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater menuInflator = getSupportMenuInflater();
		if (sender == "asterisk") {
			return true;
		} else {
			menuInflator.inflate(R.layout.menu_conversation, menu);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (sender == "asterisk") {
			return true;
		}
		switch (item.getItemId()) {
		case R.id.reply_action:
			Intent messagingIntent = new Intent(getApplicationContext(),
					MessagingActivity.class);
			messagingIntent.putExtra("recipient", sender);
			messagingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(messagingIntent);
			break;
		}
		return true;
	}

	private class GetConversationTask extends AsyncTask<String, Void, String> {

		private String result;

		protected String doInBackground(String... params) {
			result = getConversation();
			return result;
		}

		private String getConversation() {
			HttpPost httpPost = new HttpPost(
					"http://appnosystem.com/api/conversation/" + sender + "/");

			// Before make API key authentication, Android client must get the
			// API key first.
			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpPost.setHeader("Authorization", apiKey);

			String responseString = "";
			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpPost);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				Log.e("get conversation", responseString);
			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		protected void onPostExecute(String result) {
			conversationList.clear();
			adapter.notifyDataSetChanged();
			JSONArray conversations;
			try {
				conversations = new JSONArray(result);
				for (int i = 0; i < conversations.length(); i++) {
					JSONObject message = new JSONObject(conversations.get(i)
							.toString());
					JSONObject sender = message.getJSONObject("sender");
					JSONObject recipient = message.getJSONObject("recipient");

					// listItems.add(message.getString("body"));
					// 2012-08-25T18:58:48.200Z
					// Calendar sentDateTime = new GregorianCalendar();
					String sentDateTime = message.getString("created_on");
					Date dateTime = null;
					try {
						String formattedDateTime = sentDateTime.replace('T',
								' ');
						dateTime = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss.SSS")
								.parse(formattedDateTime);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Message m = null;
					if (!message.getString("agenda_json").equals("None")) {
						// server response as String
						String agendaString = message.getString("agenda_json");
						Log.e("agenda sunsun", agendaString);
						// but we need to convert String to JSONObject
						JSONObject agendaJSON = new JSONObject(agendaString);
						Log.e("agendaJSON", agendaJSON.toString());

						String appointmentTimeString = agendaJSON
								.getString("datetime");

						Date appointmentTime = null;
						try {

							appointmentTime = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss")
									.parse(appointmentTimeString);
						} catch (ParseException e) {
							e.printStackTrace();
						}

						String place = agendaJSON.getString("place");

						boolean confirm = agendaJSON.getBoolean("confirm");
						int agendaId = agendaJSON.getInt("agenda_id");
						Agenda agenda = new Agenda(appointmentTime, confirm,
								place);
						// for delete and approve
						agenda.setId(agendaId);
						m = new Message(new AnsUser(sender), new AnsUser(
								recipient), message.getString("body"),
								message.getInt("message_status"), dateTime,
								agenda);
					} else {
						m = new Message(new AnsUser(sender), new AnsUser(
								recipient), message.getString("body"),
								message.getInt("message_status"), dateTime);
					}
					String missedCallPhotoUrl = message
							.getString("missed_call_photo_url");
					m.setMissedCallPhotoUrl(missedCallPhotoUrl);
					Log.e("ConversationListActivity.GetConversationTask",
							m.getMissedCallPhotoUrl());
					conversationList.add(m);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			// ((PullToRefreshListView) getListView()).onRefreshComplete();
			listView.onRefreshComplete();
			super.onPostExecute(result);
		}
	}

	private class GetProfilePictureTask extends AsyncTask<Void, Void, String> {

		private String result;

		public String doInBackground(Void... params) {
			result = getProfilePicture();
			return result;
		}

		private String getProfilePicture() {
			HttpPost httpPost = new HttpPost("http://appnosystem.com/api/user/");
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("get_profile_picture",
					"true"));
			nameValuePairs.add(new BasicNameValuePair("username", sender));
			Log.e("WHO", sender);
			// Before make API key authentication, Android client must get the
			// API key first.
			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpPost.setHeader("Authorization", apiKey);
			String responseString = "";
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						HTTP.UTF_8));
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpPost);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				Log.e("get profile picture", responseString);
			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		public void onPostExecute(String result) {
			try {
				JSONObject senderJSONObject = new JSONObject(result);
				AnsUser sender = new AnsUser(senderJSONObject);
				imageLoader.displayImage(sender.getProfilePictureUrl(),
						senderProfilePicture);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}
}
