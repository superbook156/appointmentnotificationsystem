package com.appnosystem;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AgendaListAdapter extends BaseAdapter {

	private Activity activity;
	private List<Agenda> agenda = new ArrayList<Agenda>();
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public AgendaListAdapter(Activity activity, List<Agenda> agenda) {
		this.activity = activity;
		this.agenda = agenda;
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(this.activity.getApplicationContext());
//		String activityName = this.activity.getClass().getCanonicalName();

	}

	@Override
	public int getCount() {

		return agenda.size();
	}

	public Agenda getItem(int position) {

		return agenda.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(R.layout.agenda_list_row, null);
		}
		Agenda selectedAgenda = getItem(position);
		ImageView profilePicture = (ImageView) view
				.findViewById(R.id.list_image);
		TextView sender = (TextView) view.findViewById(R.id.sender);
		TextView detail = (TextView) view.findViewById(R.id.detail);
		TextView agendaDate = (TextView) view.findViewById(R.id.agenda_date);
		TextView agendaTime = (TextView) view.findViewById(R.id.agenda_time);
		//imageLoader.displayImage(.getSender()
			//	.getProfilePictureUrl(), profilePicture);
		//agenda_date.setText();
		//agenda_time.setText();
		//detail.setText();
		imageLoader.displayImage(selectedAgenda.getSender()
				.getProfilePictureUrl(), profilePicture);
		sender.setText(selectedAgenda.getSender().getFullname());
		detail.setText(selectedAgenda.getMessageBody());
		agendaDate.setText(selectedAgenda.getDate());
		agendaTime.setText(selectedAgenda.getTime());
		
		return view;
	}

}
