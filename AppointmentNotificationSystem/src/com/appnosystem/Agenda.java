package com.appnosystem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author ANS
 * 
 */
public class Agenda {
	// TODO
	private int id;
	private Calendar appointmentTime;
	private Date appointmentDatetime;
	private boolean confirm;
	private String place;
	private AnsUser sender;
	private AnsUser recipient;
	private String messageBody;

	public Agenda() {
		appointmentTime = Calendar.getInstance();
		confirm = false;
		place = "default";
	}

	public Agenda(Calendar appointmentTime, boolean confirm, String place) {
		this.appointmentTime = appointmentTime;
		this.confirm = confirm;
		this.place = place;
	}

	/**
	 * Book
	 * 
	 * @param appointmentTime
	 * @param confirm
	 * @param place
	 */
	public Agenda(Date appointmentTime, boolean confirm, String place) {
		this.appointmentTime = Calendar.getInstance();
		this.appointmentTime.setTime(appointmentTime);

		this.appointmentDatetime = appointmentTime;
		this.confirm = confirm;
		this.place = place;

	}

	public Calendar getAppointmentTime() {
		return this.appointmentTime;
	}

	public void setAppointmentTime() {

	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}


	public AnsUser getSender() {
		return sender;
	}
	
	public void setSender(AnsUser sender) {
		this.sender = sender;
	}
	
	public void setRecipient(AnsUser recipient) {
		this.recipient = recipient;
	}
	
	public AnsUser getRecipient() {
		return recipient;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getMessageBody() {

		return messageBody;

	}


	/**
	 * Book
	 * 
	 * @return
	 */
	public String getDate() {
		String date = new SimpleDateFormat("MM/dd").format(appointmentDatetime);

		return date;
	}

	public String getLongDate() {
		String longDate = new SimpleDateFormat("EEE, d MMM yyyy")
				.format(appointmentDatetime);
		return longDate;
	}

	/**
	 * Book
	 * 
	 * @return
	 */
	public String getTime() {
		String time = new SimpleDateFormat("HH:mm").format(appointmentDatetime);
		return time;
	}

	/**
	 * Book
	 * 
	 * @return meeting place
	 */
	public String getPlace() {
		return this.place;
	}

	public boolean getConfirmation() {
		return this.confirm;
	}

	public void setConfirmation(boolean confirm) {
		this.confirm = confirm;
	}

	public void setplace(String place) {
		this.place = place;
	}

	/*
	 * public String toString() { int minute =
	 * appointmentTime.get(Calendar.MINUTE) / DateTimeSlider.MINUTEINTERVAL *
	 * DateTimeSlider.MINUTEINTERVAL; return
	 * String.format("%n%te %tB %tY%n%tH:%02d", appointmentTime,
	 * appointmentTime, appointmentTime, appointmentTime, minute); // return
	 * String.format("%n%te %tB %tY%n %tH:%02d", // appointmentTime,
	 * appointmentTime, appointmentTime, // appointmentTime,
	 * appointmentTime.get(Calendar.MINUTE)); }
	 */
}
