package com.appnosystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class AgendaDetailActivity extends AnsBaseActivity implements
		OnClickListener{

	private TextView senderFullname;
	private TextView senderUsername;
	private TextView agendaBodyText;
	private TextView agendaDateText;
	private TextView agendaTimeText;
	private TextView agendaPlaceText;
	private TextView agendaShortDateText;
	private Button reminderBtn;

	private String fullname;
	private String username;
	private String body;
	private String agendaTimeString;
	private String agendaDateString;
	private String agendaPlace;
	private String agendaShortDate;
	private boolean agendaConfirm;

	// Book
	private Calendar agendaCalendar;
	private Date agendaDate;
	private Intent alarmIntent;
	private PendingIntent pendingIntent;
	private AlarmManager alarmManager;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.agenda_detail);
		getSupportActionBar().setTitle("Agenda Detail");

		senderFullname = (TextView) findViewById(R.id.agenda_sender_name);
		agendaBodyText = (TextView) findViewById(R.id.agenda_detail);
		agendaDateText = (TextView) findViewById(R.id.agenda_date);
		agendaTimeText = (TextView) findViewById(R.id.agenda_time);
		agendaPlaceText = (TextView) findViewById(R.id.agenda_place);
		agendaShortDateText = (TextView) findViewById(R.id.agenda_shot_date);
		reminderBtn = (Button) findViewById(R.id.reminder_agenda);
		
		reminderBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						ReminderEditActivity.class);
				startActivity(i);
				
			}
		});

		try {
			Bundle extras = getIntent().getExtras();
			fullname = extras.getString("agenda_fullname");
			body = extras.getString("agenda_body");
			agendaTimeString = extras.getString("agenda_time");
			agendaDateString = extras.getString("agenda_date");
			agendaPlace = extras.getString("agenda_place");
			agendaShortDate = extras.getString("agenda_short_date");
		} catch (Exception e) {
			e.printStackTrace();
		}
		senderFullname.setText(fullname);
		agendaBodyText.setText(body);
		agendaDateText.setText(agendaDateString);
		agendaTimeText.setText(agendaTimeString);
		agendaPlaceText.setText(agendaPlace);
		agendaShortDateText.setText(agendaShortDate);

		try {
			String agendaDateTime = agendaDateString + " " + agendaTimeString;
			SimpleDateFormat formatter;

			formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
			agendaDate = (Date) formatter.parse(agendaDateTime);

			agendaCalendar = Calendar.getInstance();
			agendaCalendar.setTime(agendaDate);

		} catch (ParseException e) {
			e.printStackTrace();
		}

	//	Spinner spinner = (Spinner) findViewById(R.id.spinner);
	//ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		//		this, R.array.reminder_arrays,
			//	android.R.layout.simple_spinner_item);
		//adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//spinner.setAdapter(adapter);
		//spinner.setOnItemSelectedListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}





}
