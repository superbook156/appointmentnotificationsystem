package com.appnosystem;

public class AnsConstants {
	public static final String ANS_PREFERENCE = "com.appnosystem";

	// Base URL of the ANS Server
	static final String SERVER_URL = "http://appnosystem.com/";

	static final String SEND_MESSAGE_URL = "http://appnosystem.com/api/send_message/";

	// Google API project id registered to use GCM.
	static final String SENDER_ID = "671081104841";

	// Tag used on log messages
	static final String TAG = "ANS";

	static final String DISPLAY_MESSAGE_ACTION = "com.gogole.android.gcm";

	static final String EXTRA_MESSAGE = "message";

	// Default values
	public static final String SIP_DOMAIN = "192.168.0.156";
}
