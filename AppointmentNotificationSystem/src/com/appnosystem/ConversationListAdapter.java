package com.appnosystem;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ConversationListAdapter extends BaseAdapter {

	private Activity activity;
	private List<Message> messages = new ArrayList<Message>();
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public ConversationListAdapter(Activity activity, List<Message> messages) {
		this.activity = activity;
		this.messages = messages;
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(this.activity.getApplicationContext());
		String activityName = this.activity.getClass().getCanonicalName();
		System.out.println(activityName);

	}

	public int getCount() {
		return messages.size();
	}

	public Message getItem(int position) {
		return messages.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(R.layout.conversation, null);
		}
		Message conversationMessage = getItem(position);
		TextView sender = (TextView) view
				.findViewById(R.id.conversation_sender_name);
		TextView detail = (TextView) view.findViewById(R.id.conversation_body);
		TextView date = (TextView) view.findViewById(R.id.conversation_date);
		TextView time = (TextView) view.findViewById(R.id.conversation_time);
		ImageView agenda = (ImageView) view.findViewById(R.id.have_agenda);

		if (conversationMessage
				.getSender()
				.getUsername()
				.equals(AnsUtilities.getUsername(activity
						.getApplicationContext()))) {
			sender.setText("Me");
		} else {
			sender.setText(conversationMessage.getSender().getFullname());
		}

		if (conversationMessage.hasAgenda()) {
			// put code here
			agenda.setImageResource(R.drawable.agenda_notify);
		} else {

		}

		detail.setText(conversationMessage.getBody());
		date.setText(conversationMessage.getDate());
		time.setText(conversationMessage.getTime());
		return view;
	}
}
