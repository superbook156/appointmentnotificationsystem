package com.appnosystem;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.RingtonePreference;
import android.view.View;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

public class AnsSettings extends SherlockPreferenceActivity implements
		OnPreferenceChangeListener {

	protected View titleView;
	private CheckBoxPreference notificationPref;
	private CheckBoxPreference vibratePref;
	private CheckBoxPreference soundPref;
	private RingtonePreference ringtonePref;
	private EditTextPreference ansUserPref;
	private Preference sipPref;

	private static final int MANAGE_SIP_ACCOUNT_CODE = 0x0001;
	// General preference key
	public static final String PREF_KEY_NOTIFICATION = "pref_key_notification";
	public static final String PREF_KEY_VIBRATE = "pref_key_vibrate";
	public static final String PREF_KEY_SOUND = "pref_key_sound";
	public static final String PREF_KEY_RINGTONE = "pref_key_ringtone";
	public static final String PREF_KEY_ANS_USER = "pref_key_ans_user";
	// SIP preference keys
	public static final String PREF_KEY_SIP = "pref_key_sip";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		getSupportActionBar().setTitle("Settings");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		notificationPref = (CheckBoxPreference) findPreference(PREF_KEY_NOTIFICATION);
		vibratePref = (CheckBoxPreference) findPreference(PREF_KEY_VIBRATE);
		soundPref = (CheckBoxPreference) findPreference(PREF_KEY_SOUND);
		ringtonePref = (RingtonePreference) findPreference(PREF_KEY_RINGTONE);

		ringtonePref.setOnPreferenceChangeListener(this);

		ansUserPref = (EditTextPreference) findPreference(PREF_KEY_ANS_USER);
		// set default value
		ansUserPref.setSummary(AnsUtilities
				.getUsername(getApplicationContext()));
		ansUserPref.setEnabled(false);
		ansUserPref.setOnPreferenceChangeListener(this);

		sipPref = (Preference) findPreference(PREF_KEY_SIP);
		sipPref.setTitle(AnsUtilities.getSipAccount(this));
		sipPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent i = new Intent();
				i.setClass(getApplicationContext(), AnsSipSettings.class);
				startActivityForResult(i, MANAGE_SIP_ACCOUNT_CODE);
				return true;
			}
		});

	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			finish();
			break;
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent extras) {
		switch (requestCode) {
		case MANAGE_SIP_ACCOUNT_CODE:
			sipPref.setTitle(AnsUtilities.getSipAccount(this));
			break;
		}
	}

	public boolean onPreferenceChange(Preference preference, Object newValue) {
		Toast.makeText(getApplicationContext(), preference.toString(),
				Toast.LENGTH_LONG).show();
		preference.setSummary(newValue.toString());
		return false;
	}

}
