package com.appnosystem;

import org.sipdroid.sipua.ui.Settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

/**
 * Utility class for ANS.
 * 
 * @author attomos
 * 
 */
public class AnsUtilities {

	public static final String ANS_PREFERENCE = "com.appnosystem";

	// Base URL of the ANS Server
	static final String SERVER_URL = "http://appnosystem.com/";

	static final String SEND_MESSAGE_URL = "http://appnosystem.com/api/send_message/";

	// Google API project id registered to use GCM.
	static final String SENDER_ID = "671081104841";

	// Tag used on log messages
	static final String TAG = "ANS";

	static final String DISPLAY_MESSAGE_ACTION = "com.gogole.android.gcm";

	static final String EXTRA_MESSAGE = "message";

	private static String backupApiKey;

	// Default values
	public static final String SIP_DOMAIN = "192.168.0.156";

	public static boolean isAuthenticated(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.contains("api_key");
	}

	public static String getProfilePicture(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("profile_picture_url", "profile_picture_url");

	}

	public static void setProfilePicture(Context context, String profilePicture) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("profile_picture_url", profilePicture);
		editor.commit();
	}

	public static String getLocation(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("location", "location");
	}

	public static void setLocation(Context context, String location) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("location", location);
		editor.commit();
	}

	public static String getUsername(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("username", "username");
	}

	public static void setUsername(Context context, String username) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("username", username);
		editor.commit();
	}

	public static String getFullName(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("fullname", "fullname");
	}

	public static void setFullName(Context context, String fullname) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("fullname", fullname);
		editor.commit();
	}

	public static String getAvailability(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("availability", "availability");
	}

	public static void setAvailability(Context context, String availability) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("availability", availability);
		editor.commit();

	}

	public static String getEmail(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("email", "email");
	}

	public static void setEmail(Context context, String email) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("email", email);
		editor.commit();
	}

	public static String getApiKey(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getString("api_key", "api_key");
	}

	public static void setApiKey(Context context, String apiKey) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("api_key", apiKey);
		editor.commit();
		// used for unregister GCM from django server
		backupApiKey = apiKey;
	}

	public static String getBackupApiKey() {
		return backupApiKey;
	}

	public static void clearBackupApiKey() {
		backupApiKey = null;
	}

	public static void clearApiKey(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.remove("api_key");
		editor.commit();
	}

	public static boolean isRegisteredOnServer(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		return prefs.getBoolean("register_on_server", false);
	}

	public static void setRegisteredOnServer(Context context,
			boolean isRegisteredOnServer) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putBoolean("register_on_server", isRegisteredOnServer);
		editor.commit();
	}

	static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}

	public static boolean checkInternetConnection(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo == null) {
			return false;
		}
		if (!networkInfo.isConnected()) {
			return false;
		}
		if (!networkInfo.isAvailable()) {
			return false;
		}
		return true;
	}

	public static String getSipAccount(Context context) {
		String sipAccount = getSipUsername(context) + "@"
				+ getSipDomain(context);
		return sipAccount;
	}

	public static String getSipUsername(Context context) {
		final SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String sipUsername = prefs.getString(
				AnsSipSettings.PREF_KEY_SIP_USERNAME, "default");
		return sipUsername;
	}

	public static void setSipUsername(Context context, String sipUsername) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(AnsSipSettings.PREF_KEY_SIP_USERNAME, sipUsername);
		editor.commit();
		// must resolve the Sipdroid settings too
		final SharedPreferences settings = context.getSharedPreferences(
				Settings.sharedPrefsFile, Context.MODE_PRIVATE);
		Editor editor2 = settings.edit();
		editor2.putString(Settings.PREF_USERNAME, sipUsername);
		editor2.commit();
	}

	public static String getSipPassword(Context context) {
		final SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String sipPassword = prefs.getString(
				AnsSipSettings.PREF_KEY_SIP_PASSWORD, "default");
		return sipPassword;
	}

	public static void setSipPassword(Context context, String sipPassword) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(AnsSipSettings.PREF_KEY_SIP_PASSWORD, sipPassword);
		editor.commit();
		// must resolve the Sipdroid settings too
		final SharedPreferences settings = context.getSharedPreferences(
				Settings.sharedPrefsFile, Context.MODE_PRIVATE);
		Editor editor2 = settings.edit();
		editor2.putString(Settings.PREF_PASSWORD, sipPassword);
		editor2.commit();
	}

	public static String getSipDomain(Context context) {
		final SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String sipDomain = prefs.getString(AnsSipSettings.PREF_KEY_SIP_DOMAIN,
				"default");
		return sipDomain;
	}

	public static void setSipDomain(Context context, String sipDomain) {
		final SharedPreferences prefs = context.getSharedPreferences(
				ANS_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString(AnsSipSettings.PREF_KEY_SIP_DOMAIN, sipDomain);
		editor.commit();
		// Must resolve the Sipdroid settings too
		// This case should be server instead of domain (for sipdroid).
		final SharedPreferences settings = context.getSharedPreferences(
				Settings.sharedPrefsFile, Context.MODE_PRIVATE);
		Editor editor2 = settings.edit();
		editor2.putString(Settings.PREF_SERVER, sipDomain);
		editor2.commit();
	}

}
