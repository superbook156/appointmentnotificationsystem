package com.appnosystem;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author ANS
 * 
 */
public class Message {

	private AnsUser sender;
	private AnsUser recipient;
	private String body;
	private int status;
	private Agenda agenda;
	private String missedCallPhotoUrl;
	Date dateTime;

	public Message() {
		this(null, null, null, 0, "");
	}

	public Message(JSONObject message) {
		try {
			// this.sender = new User(message.get("sender"));
			JSONObject e = new JSONObject("think later");
			System.out.println(e);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Currently use
	 * 
	 * @param sender
	 * @param recipient
	 * @param body
	 * @param status
	 */
	public Message(AnsUser sender, AnsUser recipient, String body, int status,
			Date dateTime) {
		this.sender = sender;
		this.recipient = recipient;
		this.body = body;
		this.status = status;
		this.dateTime = dateTime;
	}

	public Message(AnsUser sender, AnsUser recipient, String body, int status,
			String agenda) {
		this.sender = sender;
		this.recipient = recipient;
		this.body = body;
		this.status = status;
		this.agenda = new Agenda();
	}

	/**
	 * @Book
	 * @param sender
	 * @param recipient
	 * @param body
	 * @param status
	 * @param dateTime
	 * @param agenda
	 */
	public Message(AnsUser sender, AnsUser recipient, String body, int status,
			Date dateTime, Agenda agenda) {
		this.sender = sender;
		this.recipient = recipient;
		this.body = body;
		this.status = status;
		this.dateTime = dateTime;
		this.agenda = agenda;
	}

	public AnsUser getSender() {
		return sender;
	}

	public AnsUser getRecipient() {
		return recipient;
	}

	public String getBody() {
		return body;
	}

	public int getStatus() {
		return status;
	}

	public Agenda getAgenda() {
		return agenda;
	}

	public String getDate() {
		String date = new SimpleDateFormat("MM/dd").format(dateTime);
		return date;
	}

	public String getTime() {
		String time = new SimpleDateFormat("HH:mm").format(dateTime);
		return time;
	}

	public String toString() {
		return body;
	}

	public boolean hasAgenda() {
		return this.agenda != null;
	}

	public void setMissedCallPhotoUrl(String url) {
		missedCallPhotoUrl = url;
	}

	public String getMissedCallPhotoUrl() {
		return missedCallPhotoUrl;
	}

	public boolean hasMissedCallPhoto() {
		return missedCallPhotoUrl != null && !missedCallPhotoUrl.equals("");
	}
}
