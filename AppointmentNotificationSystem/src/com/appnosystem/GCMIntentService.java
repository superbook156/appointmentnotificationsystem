package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		// super()
	}

	/**
	 * Handle the receive data from GCM.
	 */
	@Override
	protected void onMessage(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
		Log.e("is agenda?", extras.containsKey("agenda") + "");
		Log.e("extras", extras.toString());
		String message = extras.getString("message");
		Log.e("message", message);
		String agenda = "";
		if (extras.containsKey("agenda")) {
			agenda = extras.getString("agenda");
			Log.e("agenda", agenda);
		}

		// server sent 5 key-value pairs, message
		// GCM with json format
		// NORMAL = 0, MISSED_CALL = 1, UNREGISTER = 2
		int notificationMode = 0;
		if (extras.containsKey("missed_call")) {
			notificationMode = 1;
		}
		generateNotification(context, message, notificationMode);
	}

	@Override
	protected void onError(Context context, String errorId) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.e("onRegisterd", registrationId);
		registerOnServer(context, registrationId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.e("onUnregistered", registrationId);
		unregisterOnServer(context);
		// generateNotification(context, "unregister GCM", 2);
		// bug, user can go to main page even if he just login 
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		return true;
	}

	public void registerOnServer(Context context, String registrationId) {
		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("register_on_server",
				"register_on_server"));
		nameValuePairs.add(new BasicNameValuePair("phone_id",
				getPhoneId(getContentResolver())));
		nameValuePairs.add(new BasicNameValuePair("registration_id",
				registrationId));
		nameValuePairs.add(new BasicNameValuePair(
				AnsServices.C2DM_ACCOUNT_EXTRA, "account_name"));
		HttpPost httpPost = new HttpPost("http://appnosystem.com/api/register/");
		httpPost.setHeader("Authorization", AnsUtilities.getApiKey(context));
		String responseString = "";
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			Log.e("register on server", responseString);
			AnsUtilities.setRegisteredOnServer(context, true);
			GCMRegistrar.setRegisteredOnServer(context, true);
		} catch (Exception e) {

		}
	}

	public void unregisterOnServer(Context context) {
		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("unregister_on_server",
				"unregister_on_server"));
		nameValuePairs.add(new BasicNameValuePair("phone_id",
				getPhoneId(getContentResolver())));
		HttpPost httpPost = new HttpPost("http://appnosystem.com/api/register/");
		// copy API key and unregister while ANS app is not authenticated

		httpPost.setHeader("Authorization", AnsUtilities.getBackupApiKey());
		String responseString = "";
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			Toast.makeText(getApplicationContext(), responseString,
					Toast.LENGTH_LONG).show();
			Log.e("unregister on server", responseString);
			AnsUtilities.setRegisteredOnServer(context, false);
			AnsUtilities.clearBackupApiKey();
			GCMRegistrar.setRegisteredOnServer(context, false);
		} catch (Exception e) {

		}
	}

	private String getPhoneId(ContentResolver contentResolver) {
		String phoneId = System.getString(contentResolver, Secure.ANDROID_ID);
		if (phoneId == null) {
			phoneId = "emulator"; // some emulators return null
		}
		return phoneId;
	}

	private static void generateNotification(Context context, String message,
			int notificationMode) {
		int icon = 0;
		switch (notificationMode) {
		case 0:
			icon = R.drawable.notification_icon;
			break;
		case 1:
			icon = R.drawable.notification_missed_call;
			break;
		case 2:
			icon = R.drawable.notification_icon;
			break;
		}

		long when = java.lang.System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		String title = context.getString(R.string.app_name);
		Intent notificationIntent = new Intent(context, MessageListActivity.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(0, notification);

		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		boolean isScreenOn = pm.isScreenOn();

		if (isScreenOn == false) {
			WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
					| PowerManager.ACQUIRE_CAUSES_WAKEUP
					| PowerManager.ON_AFTER_RELEASE, "MyLock");
			wakeLock.acquire(10000);
			WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					"MyCpuLock");
			wl_cpu.acquire(10000);
		}
	}

}
