package com.appnosystem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

/**
 * 
 * @author bookkung
 * 
 *         Prevent AlertDialog to close when button is clicked.
 * 
 */
public class MessagingActivity extends AnsBaseActivity implements
		OnClickListener, TextWatcher {
	private static final String TAG = "MessagingActivity";
	private static final int MESSAGE_BODY_LIMIT = 140;
	private EditText messageBodyEditText;
	private String recipient;
	private EditText recipientEditText;

	private EditText dateEditText;
	private EditText timeEditText;
	private EditText placeEditText;
	private Button sendMessageButton;
	private ImageView quickMessageBtn;
	private int hour;
	private int minute;
	private int year;
	private int month;
	private int day;
	static final int DATE_DIALOG = 777;
	static final int TIME_DIALOG = 888;
	private static final int PLACE_DIALOG = 999;

	private String dateValue;
	private String timeValue;

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;
			timeValue = selectedHour + ":" + selectedMinute;

			timeEditText.setText(new StringBuilder().append(pad(hour))
					.append(":").append(pad(minute)));
		}

		private Object pad(int c) {
			if (c >= 10)
				return String.valueOf(c);
			else
				return "0" + String.valueOf(c);
		}
	};

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			Date selectedDate = new Date();
			selectedDate.setYear(year);
			selectedDate.setMonth(month);
			selectedDate.setDate(day);

			if (selectedMonth < 10) {
				dateValue = selectedYear + "-0" + selectedMonth + "-"
						+ selectedDay;
			} else {
				dateValue = selectedYear + "-" + selectedMonth + "-"
						+ selectedDay;
			}
			String longDate = new SimpleDateFormat("EEE, d MMM")
					.format(selectedDate);
			dateEditText.setText(longDate + " " + year);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.messages_layout);

		getSupportActionBar().setTitle("Send Message");

		placeEditText = (EditText) findViewById(R.id.placeEditText);
		placeEditText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int inputType = placeEditText.getInputType();
				placeEditText.setInputType(InputType.TYPE_NULL);
				placeEditText.onTouchEvent(event);
				showDialog(PLACE_DIALOG);
				placeEditText.setInputType(inputType);
				return true;

			}
		});

		recipientEditText = (EditText) findViewById(R.id.recipientEditText);
		placeEditText = (EditText) findViewById(R.id.placeEditText);
		dateEditText = (EditText) findViewById(R.id.dateEditText);
		timeEditText = (EditText) findViewById(R.id.timeEditText);
		quickMessageBtn = (ImageView) findViewById(R.id.quickMessageBtn);
		quickMessageBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getListQuickMessageDialog();
				
				
			}
		});

		final Calendar c = Calendar.getInstance();
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		messageBodyEditText = (EditText) findViewById(R.id.messageBodyEditText);
		messageBodyEditText.addTextChangedListener(this);

		try {
			recipient = getIntent().getExtras().getString("recipient");
			recipientEditText.setText(recipient);
		} catch (Exception e) {
			recipient = "";
		}

		dateEditText.setOnClickListener(this);
		timeEditText.setOnClickListener(this);

		sendMessageButton = (Button) findViewById(R.id.sendMessageButton);
		sendMessageButton.setEnabled(false);
		sendMessageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String recipient = recipientEditText.getText().toString();
				String messageString = messageBodyEditText.getText().toString();

				Intent sendMessageIntent = new Intent(getApplicationContext(),
						AnsServices.class);
				sendMessageIntent.setAction(AnsServices.SEND_MESSAGE_TO);
				sendMessageIntent.putExtra("recipient", recipient);
				sendMessageIntent.putExtra("message_body", messageString);

				// have agenda attached with this message
				String placeValue = placeEditText.getText().toString();
				try {
					if (!dateValue.equals("") && !placeValue.equals("")
							&& !timeValue.equals("")) {
						sendMessageIntent.putExtra("agenda", "true");
						sendMessageIntent.putExtra("date_value", dateValue);
						sendMessageIntent.putExtra("time_value", timeValue);
						sendMessageIntent.putExtra("place", placeValue);
						Log.i(TAG, "send message with agenda");
					} else {
						// No agenda, incomplete information
						sendMessageIntent.putExtra("agenda", "false");
						Log.i(TAG, "send message without agenda");
					}
				} catch (Exception e) {
					// No agenda, blank agenda fields
					sendMessageIntent.putExtra("agenda", "false");
					Log.i(TAG, "send message without agenda");
				}
				 startService(sendMessageIntent);
				finish();
			}

		});
	}
	
	public void getListQuickMessageDialog() {
		final CharSequence[] items = {"I will be there ASAP.", "I will contact you later.","Now I'm busy."};
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Quick Message");
		builder.setIcon(R.drawable.pen_edit);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int item) {
				// TODO Auto-generated method stub
				messageBodyEditText.setText(items[item]);
				
			}
		});
		
		
		builder.show();
		
		
	}
	
	public Dialog getListPlaceDialog() {
		final CharSequence[] items = { "CB 2", "SIT Building", "LNG" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Places");
		builder.setIcon(R.drawable.ic_dialog_place);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				placeEditText.setText(items[item]);
			}
		});
		AlertDialog alert = builder.create();
		return alert;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dateEditText:
			showDialog(DATE_DIALOG);
			break;
		case R.id.timeEditText:
			showDialog(TIME_DIALOG);
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG:
			return new TimePickerDialog(this, timePickerListener, hour, minute,
					true);
		case PLACE_DIALOG:
			return getListPlaceDialog();
		case DATE_DIALOG:
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;

	}

	@Override
	public void afterTextChanged(Editable s) {
		int messageLength = messageBodyEditText.length();
		TextView bodyTextView = (TextView) findViewById(R.id.bodyTextView);
		// limited message body to 140
		if (messageLength <= MESSAGE_BODY_LIMIT && messageLength > 0) {
			sendMessageButton.setEnabled(true);
		} else {
			sendMessageButton.setEnabled(false);
		}
		bodyTextView.setText("Body [" + messageLength + "]");
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

}
