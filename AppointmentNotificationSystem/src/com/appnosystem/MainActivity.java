package com.appnosystem;

import static com.appnosystem.AnsUtilities.SENDER_ID;
import static com.appnosystem.AnsUtilities.TAG;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gcm.GCMRegistrar;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

/**
 * Main activity of ANS. This activity will create after user is authenticated.
 * 
 * @author ANS
 * 
 */
public class MainActivity extends AnsBaseActivity implements OnClickListener {

	private Button statusButton;
	private Button messagesButton;
	private Button agendaButton;
	private Button contactButton;

	private AsyncTask<Void, Void, Void> registerTask;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard_layout);
//		Crouton.makeText(this, "YES", Style.INFO).show();
		statusButton = (Button) findViewById(R.id.btn_status);
		messagesButton = (Button) findViewById(R.id.btn_messages);
		agendaButton = (Button) findViewById(R.id.btn_agenda);
		contactButton = (Button) findViewById(R.id.btn_contacts);

		// Registration process should take place after user is
		// authenticated.
		final String registrationId = GCMRegistrar.getRegistrationId(this);
		if (registrationId.equals("")) {
			Log.e(TAG, "registration_id=\"\" How come?");
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			// Device is already registered on GCM, check server.
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				Log.e(TAG, "Already registered on server");
			} else {
				// Try to register again, but not in the UI thread.
				// It's also necessary to cancel the thread onDestroy(),
				// hence the user of AsyncTask instead of raw thread.
				final Context context = this;
				registerTask = new AsyncTask<Void, Void, Void>() {

					protected Void doInBackground(Void... params) {
						boolean registered = AnsUtilities
								.isRegisteredOnServer(context);
						// At this point all attempts to register with the
						// app server failed, so we need to unregister the
						// device from GCM - the app will try to register again
						// when it is restarted. Note that GCM will send an
						// unregistered callback upon completion, but
						// GCMIntentService.onUnregistered() will ignore it.
						if (!registered) {
							GCMRegistrar.unregister(context);
						}
						Log.e(TAG, "surprise");
						return null;
					}

					@Override
					protected void onPostExecute(Void results) {
						registerTask = null;
					}
				};
				registerTask.execute(null, null, null);
			}
			Log.v(TAG, "Already registered (finally)");
		}

		statusButton.setOnClickListener(this);
		messagesButton.setOnClickListener(this);
		agendaButton.setOnClickListener(this);
		contactButton.setOnClickListener(this);

	}

	public void onClick(View v) {
		int viewId = v.getId();
		Intent i = null;
		switch (viewId) {
		case R.id.btn_status:
			i = new Intent(getApplicationContext(), StatusActivity.class);
			startActivity(i);
			break;
		case R.id.btn_messages:
			i = new Intent(getApplicationContext(), MessageListActivity.class);
			startActivity(i);
			break;
		case R.id.btn_agenda:
			i = new Intent(getApplicationContext(), AgendaListActivity.class);
			startActivity(i);
			break;
		case R.id.btn_contacts:
			i = new Intent(getApplicationContext(), ContactListActivity.class);
			startActivity(i);
			break;
		}
	}

	@Override
	protected void onDestroy() {
		if (registerTask != null) {
			registerTask.cancel(true);
		}
		// GCMRegistrar.onDestroy(this);
		super.onDestroy();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		showNoInternetInfo();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// When touch the back key, move the task containing this activity
			// to the back of the activity stack.
			moveTaskToBack(true);

			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
