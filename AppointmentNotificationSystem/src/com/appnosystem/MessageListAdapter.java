package com.appnosystem;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageListAdapter extends BaseAdapter {

	private Activity activity;
	private List<Message> messages = new ArrayList<Message>();
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public MessageListAdapter(Activity activity, List<Message> messages) {
		this.activity = activity;
		this.messages = messages;
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(this.activity.getApplicationContext());
		String activityName = this.activity.getClass().getCanonicalName();

		Log.i("MessageListAdapter", activityName);
	}

	public int getCount() {
		return messages.size();
	}

	public Message getItem(int position) {
		return messages.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(R.layout.message_list_row, null);
		}
		TextView status = (TextView) view.findViewById(R.id.status_message);
		Message selectedMessage = getItem(position);
		if (getItem(position).getStatus() == 1) {
			status.setBackgroundResource(R.color.red);
		} else {
			status.setBackgroundResource(R.color.green);
		}
		// TextView status = (TextView) view.findViewById(R.id.status_message);
		ImageView profilePicture = (ImageView) view
				.findViewById(R.id.list_image);

		// Not a real sender, this is just a header to indicate
		// you are talking with whom.
		TextView sender = (TextView) view.findViewById(R.id.sender);
		TextView detail = (TextView) view.findViewById(R.id.detail);
		TextView date = (TextView) view.findViewById(R.id.date);
		TextView time = (TextView) view.findViewById(R.id.time);
		// status.setText(messages.get(position).getStatus() + "");

		// if the latest sender of the selected message is current user
		// sender (header) should be another.

		if (AnsUtilities.getUsername(activity.getApplicationContext()).equals(
				selectedMessage.getSender().getUsername())) {
			imageLoader.displayImage(selectedMessage.getRecipient()
					.getProfilePictureUrl(), profilePicture);
			sender.setText(selectedMessage.getRecipient().toString());
		} else {
			imageLoader.displayImage(selectedMessage.getSender()
					.getProfilePictureUrl(), profilePicture);
			sender.setText(selectedMessage.getSender().toString());
		}
		detail.setText(selectedMessage.getBody());
		date.setText(selectedMessage.getDate());
		time.setText(selectedMessage.getTime());
		return view;
	}

}