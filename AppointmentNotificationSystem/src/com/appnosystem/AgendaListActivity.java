package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

public class AgendaListActivity extends AnsBaseListActivity implements
		OnRefreshListener<ListView> {
	private AgendaListAdapter adapter;
	private List<Agenda> agendaList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Agenda");
		agendaList = new ArrayList<Agenda>();
		listView.setOnRefreshListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// Agenda selectedAgenda = ((Agenda) listView
				// .getItemAtPosition(position));

				// position - 1 because it start from header
				Agenda selectedAgenda = agendaList.get(position - 1);
				Intent detailIntent = new Intent(getApplicationContext(),
						AgendaDetailActivity.class);
				detailIntent.putExtra("agenda_fullname", selectedAgenda
						.getSender().getFullname());
				detailIntent.putExtra("agenda_body",
						selectedAgenda.getMessageBody());
				detailIntent.putExtra("agenda_date",
						selectedAgenda.getLongDate());
				detailIntent.putExtra("agenda_time", selectedAgenda.getTime());
				detailIntent.putExtra("agenda_place", selectedAgenda.getPlace());
				detailIntent.putExtra("agenda_short_date",
						selectedAgenda.getDate());
				startActivity(detailIntent);

			}
		});
		adapter = new AgendaListAdapter(this, agendaList);
		listView.setAdapter(adapter);
		listView.setRefreshing();
		new GetAgendaTask().execute();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		super.onRefresh(refreshView);
		new GetAgendaTask().execute();
	}

	private class GetAgendaTask extends AsyncTask<String, Void, String> {

		private String result;

		@Override
		protected String doInBackground(String... params) {
			// http://appnosystem.com/api/agenda/
			result = getAgendaList();
			return result;
		}

		private String getAgendaList() {
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
//			nameValuePairs
//					.add(new BasicNameValuePair("latest_messages", "true"));
			HttpPost httpPost = new HttpPost(
					"http://appnosystem.com/api/agenda/");

			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpPost.setHeader("Authorization", apiKey);

			String responseString = "";
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						HTTP.UTF_8));
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpPost);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				 Log.e("AgendaListActivity.GetAgendaTask", responseString);

			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		protected void onPostExecute(String result) {
			agendaList.clear();
			adapter.notifyDataSetChanged();
			JSONArray agendas;

			try {
				agendas = new JSONArray(result);
				for (int i = 0; i < agendas.length(); i++) {
					JSONObject agenda = new JSONObject(agendas.get(i)
							.toString());

					JSONObject message = agenda.getJSONObject("message_id");
					JSONObject sender = message.getJSONObject("sender");
					// JSONObject recipient = agenda.getJSONObject("recipient");
					String agendaDatetime = agenda.getString("datetime");
					// JSONObject agenda_detail =
					// agenda.getJSONObject("agenda_json");
					boolean confirm = agenda.getBoolean("confirm");
					String place = agenda.getString("place");
					Date appointmentTime = null;
					try {
						String formattedDateTime = agendaDatetime.replace('T',
								' ');
						appointmentTime = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss").parse(formattedDateTime);
						Log.e("appointmentTime", appointmentTime.toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Agenda a = new Agenda(appointmentTime, confirm, place);
					a.setSender(new AnsUser(sender));
					a.setMessageBody(message.getString("body"));
					agendaList.add(a);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			listView.onRefreshComplete();
			super.onPostExecute(result);
		}

	}
}
