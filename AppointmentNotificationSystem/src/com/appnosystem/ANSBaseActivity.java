package com.appnosystem;

import org.sipdroid.sipua.ui.Settings;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gcm.GCMRegistrar;

import de.neofonie.mobile.app.android.widget.crouton.Crouton;
import de.neofonie.mobile.app.android.widget.crouton.Style;

public class AnsBaseActivity extends SherlockActivity {

	protected ProgressDialog progressDialog;
	protected Account selectedAccount;
	protected View titleView;

	private static AnsBaseActivity ansReference;

	public static final int ACTIVITY_ACCOUNTS = 1;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		progressDialog = new ProgressDialog(this);
		ansReference = this;
		showNoInternetInfo();
		// remove when publish
		// GCMRegistrar.checkDevice(this);
		// GCMRegistrar.checkManifest(this);
	}

	private Account getAccountFromAccountName(String accountName) {
		AccountManager accountManager = AccountManager.get(this);
		Account accounts[] = accountManager.getAccounts();
		for (int i = 0; i < accounts.length; ++i)
			if (accountName.equals(accounts[i].name))
				return accounts[i];
		return null;
	}

	public static AnsBaseActivity getReference() {
		return ansReference;
	}

	public Account getSelectedAccount() {
		return selectedAccount;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent extras) {
		super.onActivityResult(requestCode, resultCode, extras);
		// in case of press back button
		if (resultCode == RESULT_CANCELED) {
			return;
		}
		switch (requestCode) {
		case ACTIVITY_ACCOUNTS: {
			String accountName = extras.getStringExtra("account");
			selectedAccount = getAccountFromAccountName(accountName);
			Toast.makeText(this, "Account selected: " + accountName,
					Toast.LENGTH_SHORT).show();
			if (selectedAccount != null) {
				// Register here maybe
			}
		}
			break;
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// no menu for LoginActivity
		if (getClass().getSimpleName().equals("LoginActivity")) {
			return true;
		}
		MenuInflater menuInflater = getSupportMenuInflater();
		menuInflater.inflate(R.layout.menu, menu);
		return true;

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.setting_menu:
			// Intent settingIntent = new Intent(getApplicationContext(),
			// AnsSettings.class);
			Intent settingIntent = new Intent(getApplicationContext(),
					Settings.class);
			startActivity(settingIntent);
			return true;
		case R.id.about_us_menu:
			// Toast.makeText(getApplicationContext(), "AboutUs is selected",
			// Toast.LENGTH_SHORT).show();
			Crouton.makeText(this, "ANS", Style.ALERT).show();
			return true;
		case R.id.select_account_menu:
			Intent i = new Intent(getApplicationContext(), AccountList.class);
			startActivityForResult(i, ACTIVITY_ACCOUNTS);
			return true;
		case R.id.logout_menu:
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setTitle("Log out");
			alertDialog.setMessage("Logging out will remove your ANS data "
					+ "from this device. Do you want to logout?");
			alertDialog.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							logoutThenLogin();
						}
					});
			alertDialog.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});
			alertDialog.show();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void showNoInternetInfo() {
		if (!AnsUtilities.checkInternetConnection(this)) {
			Crouton.makeText(this, "No internet connection. Please try again",
					Style.INFO).show();
		}
	}

	private void logoutThenLogin() {
		// New log out flow
		Toast.makeText(this, "unregister GCM > clear API key > log out",
				Toast.LENGTH_LONG).show();

		// TODO study about GCM register/unregister
		// unregister here maybe
		GCMRegistrar.unregister(this);
		AnsUtilities.clearApiKey(this);

		Intent loginIntent = new Intent(getApplicationContext(),
				LoginActivity.class);
		loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(loginIntent);
	}

}
