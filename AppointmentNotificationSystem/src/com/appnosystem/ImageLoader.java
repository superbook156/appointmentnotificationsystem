package com.appnosystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

public class ImageLoader {

	private MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;
	private static final int stubImage = R.drawable.profile_pic;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;

	public ImageLoader(Context context) {
		fileCache = new FileCache(context);
		executorService = Executors.newFixedThreadPool(5);
	}

	public void displayImage(String url, ImageView imageView) {
		imageViews.put(imageView, url);
		Bitmap bitmap = memoryCache.getBitmap(url);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			queuePhoto(url, imageView);
			imageView.setImageResource(stubImage);
		}
	}

	private void queuePhoto(String url, ImageView imageView) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null) {
			return b;
		}

		// from web
		try {
			Bitmap bitmap = null;
			if (!url.startsWith("http://")) {
				url = "http://" + url;
			}

			URL imageUrl = new URL(url); // TODO
			HttpURLConnection connection = (HttpURLConnection) imageUrl
					.openConnection();
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);
			connection.setInstanceFollowRedirects(true);
			InputStream inputStream = connection.getInputStream();
			OutputStream outputStream = new FileOutputStream(f);
			BitmapUtils.copyStream(inputStream, outputStream);
			outputStream.close();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Throwable ex) {
			ex.printStackTrace();
			if (ex instanceof OutOfMemoryError) {
				memoryCache.clear();
			}
			return null;
		}
	}

	/**
	 * Decodes image and scales it to reduce memory consumption
	 * 
	 * @param file
	 * @return
	 */
	private Bitmap decodeFile(File file) {
		try {
			// decode image size
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory
					.decodeStream(new FileInputStream(file), null, options);

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 70;
			int widthTmp = options.outWidth;
			int heightTmp = options.outHeight;
			int scale = 1;
			while (true) {
				if (widthTmp / 2 < REQUIRED_SIZE
						|| heightTmp / 2 < REQUIRED_SIZE) {
					break;
				}
				widthTmp /= 2;
				heightTmp /= 2;
				scale *= 2;
			}

			// decode with inSampleSize
			BitmapFactory.Options options2 = new BitmapFactory.Options();
			options2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(file), null,
					options2);
		} catch (FileNotFoundException e) {

		}
		return null;
	}

	private boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url)) {
			return true;
		}
		return false;
	}

	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String url, ImageView imageView) {
			this.url = url;
			this.imageView = imageView;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		public PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			if (imageViewReused(photoToLoad)) {
				return;
			}
			Bitmap bitmap = getBitmap(photoToLoad.url);
			memoryCache.putBitmap(photoToLoad.url, bitmap);
			if (imageViewReused(photoToLoad)) {
				return;
			}
			BitmapDisplayer bitmapDisplayer = new BitmapDisplayer(bitmap,
					photoToLoad);
			Activity activity = (Activity) photoToLoad.imageView.getContext();
			activity.runOnUiThread(bitmapDisplayer);
		}

	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap bitmap, PhotoToLoad photoToLoad) {
			this.bitmap = bitmap;
			this.photoToLoad = photoToLoad;
		}

		public void run() {
			if (imageViewReused(photoToLoad)) {
				return;
			}
			if (bitmap != null) {
				photoToLoad.imageView.setImageBitmap(bitmap);
			} else {
				photoToLoad.imageView.setImageResource(stubImage);
			}
		}
	}


	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}
}
