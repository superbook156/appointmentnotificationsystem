package com.appnosystem;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactListAdapter extends BaseAdapter {

	private Activity activity;
	private List<AnsUser> users = new ArrayList<AnsUser>();
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public ContactListAdapter(Activity activity, List<AnsUser> users) {
		this.activity = activity;
		this.users = users;
		inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(this.activity.getApplicationContext());
	}

	public int getCount() {
		return users.size();
	}

	public AnsUser getItem(int position) {
		return users.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(R.layout.contact_list_row, null);
		}
		// User u = new User(username, asteriskUsername, firstName,
		// lastName, email, status, userType);
		ImageView profilePicture = (ImageView) view
				.findViewById(R.id.profile_pic);
		TextView fullname = (TextView) view.findViewById(R.id.fullname);
		TextView availabilityText = (TextView) view
				.findViewById(R.id.availability_text);
		TextView placeText = (TextView) view.findViewById(R.id.place_text);
		ImageView status = (ImageView) view.findViewById(R.id.status_symbol);

		AnsUser selectedUser = getItem(position);

		imageLoader.displayImage(selectedUser.getProfilePictureUrl(),
				profilePicture);

		fullname.setText(selectedUser.toString());
		availabilityText.setText(selectedUser.getAvailability());
		placeText.setText("Earth");
		Resources resources = availabilityText.getResources();
		if (selectedUser.getStatus() == 0) {
			status.setImageResource(R.drawable.online_status);
			availabilityText.setTextColor(resources.getColor(R.color.online));
		} else if (selectedUser.getStatus() == 1) {
			status.setImageResource(R.drawable.busy_status);
			availabilityText.setTextColor(resources.getColor(R.color.busy));
		} else {
			status.setImageResource(R.drawable.appear_offline_status);
			availabilityText.setTextColor(resources.getColor(R.color.offline));
		}
		return view;
	}
}
