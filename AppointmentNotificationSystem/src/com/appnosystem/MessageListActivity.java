package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

public class MessageListActivity extends AnsBaseListActivity implements
		OnRefreshListener<ListView> {
	private List<Message> messagesList;
	private MessageListAdapter adapter;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setTitle("Message");

		messagesList = new ArrayList<Message>();
		adapter = new MessageListAdapter(this, messagesList);
		listView.setOnRefreshListener(this);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// position - 1 because it start from header
				Log.e("onItemClick", messagesList.get(position - 1).toString());
				Message clickedMessage = messagesList.get(position - 1);
				String sender = clickedMessage.getSender().getUsername();

				if (sender.equals(AnsUtilities
						.getUsername(getApplicationContext()))) {
					sender = clickedMessage.getRecipient().getUsername();
				}
				Intent conversationIntent = new Intent(getApplicationContext(),
						ConversationListActivity.class);
				conversationIntent.putExtra("sender", sender);
				startActivity(conversationIntent);
			}
		});

		listView.setAdapter(adapter);
		listView.setRefreshing();
		new GetMessagesTask().execute();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		super.onRefresh(refreshView);
		new GetMessagesTask().execute();
	}

	private class GetMessagesTask extends AsyncTask<String, Void, String> {

		private String result;

		@Override
		protected String doInBackground(String... params) {
			result = getLatestMessages();
			return result;
		}

		private String getLatestMessages() {
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			nameValuePairs
					.add(new BasicNameValuePair("latest_messages", "true"));

			HttpPost httpPost = new HttpPost(
					"http://appnosystem.com/api/messages/");

			// Before make API key authentication, Android client must get the
			// API key first.
			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpPost.setHeader("Authorization", apiKey);

			String responseString = "";
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						HTTP.UTF_8));
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpPost);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				Log.e("MessageListActivity", responseString);
			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {
			messagesList.clear();
			adapter.notifyDataSetChanged();
			JSONArray messages;
			// Clear list before add message to it
			// listItems.clear();
			try {
				messages = new JSONArray(result);
				// Log.e("nice", messages.getString(""));
				for (int i = 0; i < messages.length(); i++) {
					JSONObject message = new JSONObject(messages.get(i)
							.toString());
					// System.out.println(message);
					JSONObject sender = message.getJSONObject("sender");
					JSONObject recipient = message.getJSONObject("recipient");

					// listItems.add(message.getString("body"));
					// 2012-08-25T18:58:48.200Z
					// Calendar sentDateTime = new GregorianCalendar();
					String sentDateTime = message.getString("created_on");
					Date dateTime = null;
					try {
						String formattedDateTime = sentDateTime.replace('T',
								' ');
						dateTime = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss.SSS")
								.parse(formattedDateTime);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Message m = new Message(new AnsUser(sender), new AnsUser(
							recipient), message.getString("body"),
							message.getInt("message_status"), dateTime);

					messagesList.add(m);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			listView.onRefreshComplete();
			// ((PullToRefreshListView) getListView()).onRefreshComplete();
			super.onPostExecute(result);
		}

	}
}