package com.appnosystem;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {

	private static int NOTIFICATION_ID = 1;

	@Override
	public void onReceive(Context context, Intent intent) {

		try {

			NotificationManager nm = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

			int icon = R.drawable.notification_icon;
			CharSequence tickerText = "Agenda Aleart";
			long when = System.currentTimeMillis();
			CharSequence contentTitle = "Vehicles Manager : rappel !";
			CharSequence contentText = "Pressez pour voir la liste des rappels";

			Intent notificationIntent = new Intent(context,
					AgendaDetailActivity.class);
			PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
					notificationIntent, 0);

			Notification notification = new Notification(icon, tickerText, when);
			notification.setLatestEventInfo(context, contentTitle, contentText,
					contentIntent);
			notification.defaults |= Notification.DEFAULT_SOUND;
			notification.defaults |= Notification.DEFAULT_VIBRATE;
			notification.flags = Notification.FLAG_ONLY_ALERT_ONCE;
			nm.notify(0, notification);

		} catch (Exception exception) {
			Toast.makeText(
					context,
					"Vehicle's Manager : erreur lors du lancement d'une notification",
					Toast.LENGTH_LONG).show();
			Log.e("ALARM_RECEIVER", exception.toString());
		}

	}

}
