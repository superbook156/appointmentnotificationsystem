package com.appnosystem;

import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListActivity;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class AnsBaseListActivity extends SherlockListActivity implements OnRefreshListener<ListView> {
	protected View titleView;
	protected PullToRefreshListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ptr_list);
		listView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
	}
	
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		listView.setLastUpdatedLabel(DateUtils.formatDateTime(
				getApplicationContext(), System.currentTimeMillis(),
				DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE
						| DateUtils.FORMAT_ABBREV_ALL));
	}
}
