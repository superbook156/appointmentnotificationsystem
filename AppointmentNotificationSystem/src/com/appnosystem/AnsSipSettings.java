package com.appnosystem;

import org.sipdroid.sipua.ui.Receiver;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.text.InputType;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

public class AnsSipSettings extends SherlockPreferenceActivity implements
		OnPreferenceChangeListener {

	private EditTextPreference sipUsernamePref;
	private EditTextPreference sipPasswordPref;
	private EditTextPreference sipDomainPref;
	private Preference sipRestoreDefaultPref;

	public static final String PREF_KEY_SIP_USERNAME = "pref_key_sip_username";
	public static final String PREF_KEY_SIP_PASSWORD = "pref_key_sip_password";
	public static final String PREF_KEY_SIP_DOMAIN = "pref_key_sip_domain";
	public static final String PREF_KEY_SIP_DEFAULT = "pref_key_sip_default";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.sip_preferences);

		getSupportActionBar().setTitle("SIP Settings");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		sipUsernamePref = (EditTextPreference) findPreference(PREF_KEY_SIP_USERNAME);
		sipUsernamePref.getEditText().setInputType(
				InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		sipUsernamePref.setPersistent(true);
		sipUsernamePref.setSummary(AnsUtilities
				.getSipUsername(getApplicationContext()));
		sipUsernamePref.setOnPreferenceChangeListener(this);

		sipPasswordPref = (EditTextPreference) findPreference(PREF_KEY_SIP_PASSWORD);
		sipPasswordPref.getEditText().setInputType(
				InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_PASSWORD);
		sipPasswordPref.setPersistent(true);
		sipPasswordPref.setOnPreferenceChangeListener(this);

		sipDomainPref = (EditTextPreference) findPreference(PREF_KEY_SIP_DOMAIN);
		sipDomainPref.getEditText().setInputType(
				InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		sipDomainPref.setPersistent(true);
		sipDomainPref.setSummary(AnsUtilities
				.getSipDomain(getApplicationContext()));
		sipDomainPref.setOnPreferenceChangeListener(this);

		sipRestoreDefaultPref = findPreference(PREF_KEY_SIP_DEFAULT);
		sipRestoreDefaultPref
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						restoreDefault();
						return true;
					}
				});
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			finish();
			break;
		}
		return true;
	}

	public boolean onPreferenceChange(Preference preference, Object newValue) {
		String prefKey = preference.getKey();
		if (prefKey.equals(PREF_KEY_SIP_PASSWORD)) {
			// For the password
			AnsUtilities.setSipPassword(getApplicationContext(),
					newValue.toString());
			return true;
		} else if (prefKey.equals(PREF_KEY_SIP_USERNAME)) {
			AnsUtilities.setSipUsername(getApplicationContext(),
					newValue.toString());

		} else if (prefKey.equals(PREF_KEY_SIP_DOMAIN)) {
			AnsUtilities.setSipDomain(getApplicationContext(),
					newValue.toString());

		}
		preference.setSummary(newValue.toString());
		Receiver.engine(this).halt();
		Receiver.engine(this).StartEngine();
		return true;
	}

	private void restoreDefault() {
		String sipUsername = AnsUtilities.getUsername(getApplicationContext());
		String sipDomain = AnsUtilities.SIP_DOMAIN;
		sipUsernamePref.setSummary(sipUsername);
		sipUsernamePref.setText(sipUsername);
		AnsUtilities.setSipUsername(getApplicationContext(), sipUsername);

		sipDomainPref.setSummary(sipDomain);
		sipDomainPref.setText(sipDomain);
		AnsUtilities.setSipDomain(getApplicationContext(), sipDomain);
		Receiver.engine(this).halt();

	}

}
