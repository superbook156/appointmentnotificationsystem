package com.appnosystem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ConversationDetailActivity extends AnsBaseActivity implements
		OnClickListener {

	private TextView senderFullname;
	private TextView senderUsername;
	private TextView conversationBody;
	// message send date and time
	private TextView conversationDateText;
	private TextView conversationTimeText;
	// agenda date, time and place
	private TextView agendaDateText;
	private TextView agendaTimeText;
	private TextView agendaPlaceText;
	private Button confirmButton;
	private ImageView missedcallPhoto;

	private String fullname;
	private String username;
	private String body;
	private String conversationDate;
	private String conversationTime;
	private String agendaDate;
	private String agendaTime;
	private String agendaPlace;
	private String missedCallPhotoUrl;
	private boolean agendaConfirm;
	private int agendaId;
	private boolean hasAgenda;

	private ImageLoader imageLoader;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conversation_detail);

		getSupportActionBar().setTitle("Detail");

		// TODO should be a sender name of selected message

		senderFullname = (TextView) findViewById(R.id.conversation_sender_name);
		senderUsername = (TextView) findViewById(R.id.conversation_sender_username);
		conversationBody = (TextView) findViewById(R.id.conversation_body);
		conversationDateText = (TextView) findViewById(R.id.conversation_date);
		conversationTimeText = (TextView) findViewById(R.id.conversation_time);
		agendaDateText = (TextView) findViewById(R.id.agenda_date);
		agendaTimeText = (TextView) findViewById(R.id.agenda_time);
		agendaPlaceText = (TextView) findViewById(R.id.agenda_place);
		confirmButton = (Button) findViewById(R.id.confirm_agenda);
		missedcallPhoto = (ImageView) findViewById(R.id.missed_call_photo);

		confirmButton.setOnClickListener(this);
		try {
			Bundle extras = getIntent().getExtras();
			fullname = extras.getString("conversation_fullname");
			username = extras.getString("username");
			body = extras.getString("conversation_body");
			conversationDate = extras.getString("conversation_date");
			conversationTime = extras.getString("conversation_time");
			agendaId = extras.getInt("agenda_id");
			agendaDate = extras.getString("agenda_date");
			agendaTime = extras.getString("agenda_time");
			agendaPlace = extras.getString("agenda_place");
			agendaConfirm = extras.getBoolean("agenda_confirm");
			hasAgenda = extras.getBoolean("has_agenda");
			missedCallPhotoUrl = extras.getString("missed_call_photo_url");
			Log.e("try this", missedCallPhotoUrl);
		} catch (Exception e) {

		}

		senderFullname.setText(fullname);
		senderUsername.setText(username);
		conversationBody.setText(body);
		conversationDateText.setText(conversationDate);
		conversationTimeText.setText(conversationTime);
		agendaDateText.setText(agendaDate);
		agendaTimeText.setText(agendaTime);
		agendaPlaceText.setText(agendaPlace);

		if (AnsUtilities.getUsername(getApplicationContext()).equals(username)
				&& hasAgenda) {
			if (agendaConfirm == false) {
				confirmButton.setText("Pending..");
			} else {
				confirmButton.setText("Confirmed");
			}
			confirmButton.setEnabled(false);
		} else if (hasAgenda && agendaConfirm == false) {
			confirmButton.setEnabled(true);
		} else if (hasAgenda && agendaConfirm == true) {
			confirmButton.setText("Confirmed");
			confirmButton.setEnabled(false);
		} else {
			View agendaDiv = findViewById(R.id.agenda_div);
			agendaDiv.setVisibility(View.GONE);
			confirmButton.setVisibility(View.GONE);

		}
		Log.e("Con de", "99");
		Log.e("WTF", missedCallPhotoUrl);
		// only missed call photo
		if (!missedCallPhotoUrl.equals("")) {
			imageLoader = new ImageLoader(this);
			imageLoader.displayImage(missedCallPhotoUrl, missedcallPhoto);
			Log.e("ConversationDetailActivity", "SO?");
			Log.e("Con de", "if");
		} else {
			missedcallPhoto.setVisibility(ImageView.GONE);
			Log.e("Con de", "else");
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.confirm_agenda:
			Log.e("ConversationDetailActivity", "confirm the agenda");
			Toast.makeText(getApplicationContext(), "Confirm agenda",
					Toast.LENGTH_LONG).show();
			new ApproveAgendaTask().execute();
			break;
		}
		// TODO Auto-generated method stub
		Toast.makeText(getApplicationContext(), "Update to Agenda",
				Toast.LENGTH_SHORT).show();
	}

	private class ApproveAgendaTask extends AsyncTask<String, Void, String> {

		private String result;

		@Override
		protected String doInBackground(String... params) {
			// http://appnosystem.com/api/agenda/
			result = approveAgenda();
			return result;
		}

		private String approveAgenda() {
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			nameValuePairs
					.add(new BasicNameValuePair("approve_agenda", "true"));
			nameValuePairs.add(new BasicNameValuePair("agenda_id", String
					.valueOf(agendaId)));
			HttpPost httpPost = new HttpPost(
					"http://appnosystem.com/api/agenda/");

			String apiKey = AnsUtilities.getApiKey(getApplicationContext());
			httpPost.setHeader("Authorization", apiKey);

			String responseString = "";
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						HTTP.UTF_8));
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(httpPost);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				Log.e("AgendaListActivity.GetAgendaTask", responseString);

			} catch (ClientProtocolException e) {

			} catch (IOException e) {

			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String result) {
			confirmButton.setText("Confirmed");
			confirmButton.setEnabled(false);
		}
	}
}
