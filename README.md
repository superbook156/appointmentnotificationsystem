#Appointment Notification System

This will change to information about code instead.

####READ ME


####Add file(s) to branch <branch_name>

    git add .
    git ci -m 'commit message'
    git push origin <branch_name>

####Switching between branches

    git checkout <branch_name>

####Remove some files on local

    git rm <file_name1>, <file_name2>
    git ci -m 'just delete ...'
    git push origin book

####Releases

[beta 0.5](https://bitbucket.org/superbook156/appointmentnotificationsystem/changeset/f26b7778acab136598a6f03145867dcbfebd3075)  
[beta 0.6](https://bitbucket.org/superbook156/appointmentnotificationsystem/downloads/ANS-0.6.apk)  
[beta 0.7](https://bitbucket.org/superbook156/appointmentnotificationsystem/downloads/ANS-0.7.apk)
